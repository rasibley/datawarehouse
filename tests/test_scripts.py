"""Test the scripts module."""

import os
from types import SimpleNamespace

import responses

from django import test
from datawarehouse import models, scripts, settings


class ScriptsTestCase(test.TestCase):
    """Unit tests for the scripts module."""

    TESTS_DIR = os.path.dirname(os.path.abspath(__file__))

    @responses.activate
    def test_bootstrap_projects_empty(self):
        """The projects table is populated if empty."""
        self._mock_projects()

        self.assertEqual(scripts.bootstrap_projects(), {
            'bootstrapped': True,
            'projects': ['cki-project/brew-pipeline', 'cki-project/pw1-pipeline', 'cki-project/cki-pipeline']})
        self.assertEqual(models.Project.objects.count(), 3)

    @responses.activate
    def test_bootstrap_projects_populated(self):
        """The projects table is not populated if non-empty."""
        self._mock_projects()

        models.Project.objects.create(project_id=1, path='dummy-pipeline')
        self.assertEqual(scripts.bootstrap_projects(), {
            'bootstrapped': False,
            'projects': ['dummy-pipeline']})
        self.assertEqual(models.Project.objects.count(), 1)

    def _mock_projects(self):
        mock_projects = os.path.join(self.TESTS_DIR, 'gitlab-projects.json')
        with open(mock_projects, 'r') as body:
            responses.add(responses.GET, url=settings.GITLAB_URL + '/api/v4/projects',
                          body=body.read())

    def test_should_ignore_pipeline(self):
        """Test should_ignore_pipeline returns correct value."""
        testcases = [
            (True, SimpleNamespace(commit={'message': "Retrigger: bla bla"}), []),
            (False, SimpleNamespace(commit={'message': "Patch: bla bla"}), []),
            (True, SimpleNamespace(commit={'message': "Patch: bla bla"}),
             [SimpleNamespace(key='retrigger', value='true')]),
            (False, SimpleNamespace(commit={'message': "Patch: bla bla"}),
             [SimpleNamespace(key='retrigger', value='off')]),
        ]

        for expected, testcase, variables in testcases:
            self.assertEqual(
                expected,
                scripts.should_ignore_pipeline(testcase, variables)
                )
