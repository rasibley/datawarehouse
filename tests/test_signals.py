"""Test the signals module."""
from unittest import mock
from freezegun import freeze_time

from django import test

from datawarehouse import models, signals


class SignalsTest(test.TestCase):
    """Unit tests for the Signals module."""

    @staticmethod
    def _create_pipeline():
        """Create and return pipeline."""
        project, _ = models.Project.objects.get_or_create(project_id=0)
        gittree, _ = models.GitTree.objects.get_or_create(name='Tree 1')
        pipeline_id = models.Pipeline.objects.count() + 1
        pipeline = models.Pipeline.objects.create(
            pipeline_id=pipeline_id,
            project=project,
            gittree=gittree,
        )
        return pipeline

    @staticmethod
    def _create_action(name, pipeline):
        """Create and return action."""
        kind, _ = models.ActionKind.objects.get_or_create(name=name)
        signal = models.Action.objects.create(kind=kind, pipeline=pipeline)
        return signal

    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.signals.utils.send_pipeline_notification')
    def test_send_pipeline_message_running(self, send_pipeline_notification):
        """Test that the pipeline message is sent correctly. Status running."""
        pipeline = self._create_pipeline()
        signals.send_pipeline_message(pipeline=pipeline)

        send_pipeline_notification.assert_called_with({
            'timestamp': '2010-01-02T09:00:00+00:00',
            'status': 'running',
            'pipeline_id': pipeline.pipeline_id,
        })

    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.signals.utils.send_pipeline_notification')
    def test_send_pipeline_message_finished(self, send_pipeline_notification):
        """Test that the pipeline message is sent correctly. Status finished."""
        pipeline = self._create_pipeline()
        pipeline.finished_at = '2010-01-02T09:00:00+00:00'
        signals.send_pipeline_message(pipeline=pipeline)

        send_pipeline_notification.assert_called_with({
            'timestamp': '2010-01-02T09:00:00+00:00',
            'status': 'finished',
            'pipeline_id': pipeline.pipeline_id,
        })

    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.signals.utils.send_pipeline_notification')
    def test_ready_to_report_message(self, send_pipeline_notification):
        """Test that the report message is sent correctly."""
        pipeline = self._create_pipeline()
        action = self._create_action('triaged', pipeline)
        signals.send_ready_to_report_message(action=action)

        send_pipeline_notification.assert_called_with(
            {
                'timestamp': '2010-01-02T09:00:00+00:00',
                'status': 'ready_to_report',
                'pipeline_id': pipeline.pipeline_id,
            }
        )

    @mock.patch('datawarehouse.signals.utils.send_pipeline_notification')
    def test_ready_to_report_message_multiple_triage(self, send_pipeline_notification):
        """Test that the report message is sent only on the first triage."""
        pipeline = self._create_pipeline()
        action = self._create_action('triaged', pipeline)
        signals.send_ready_to_report_message(action=action)

        self.assertEqual(1, send_pipeline_notification.call_count)

        # Another triage action doesn't send message.
        action = self._create_action('triaged', pipeline)
        signals.send_ready_to_report_message(action=action)

        self.assertEqual(1, send_pipeline_notification.call_count)

    @mock.patch('datawarehouse.signals.utils.send_pipeline_notification')
    def test_ready_to_report_message_wrong_action(self, send_pipeline_notification):
        """Test that the report message is sent only with triage action."""
        pipeline = self._create_pipeline()
        action = self._create_action('something', pipeline)
        signals.send_ready_to_report_message(action=action)

        self.assertEqual(0, send_pipeline_notification.call_count)

    @mock.patch('datawarehouse.signals.utils.send_pipeline_notification')
    def test_ready_to_report_message_wrong_action_previously_triaged(self, send_pipeline_notification):
        """Test that the report message is sent only with triage action even if it was traiged before."""
        pipeline = self._create_pipeline()
        self._create_action('triaged', pipeline)
        action = self._create_action('something', pipeline)
        signals.send_ready_to_report_message(action=action)

        self.assertEqual(0, send_pipeline_notification.call_count)
