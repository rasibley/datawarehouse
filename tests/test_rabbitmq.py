"""Test the rabbitmq module."""
from unittest import mock
from unittest.mock import patch, call
import pika

from django import test

from datawarehouse import rabbitmq, models


class MessageQueueTest(test.TestCase):
    """Unit tests for the MessageQueue module."""

    def setUp(self):
        """Set Up."""
        self.messagequeue = rabbitmq.MessageQueue('host', 123)
        self.messages = [
            (f'body_{index}', f'exchange_{index}') for index in range(3)
        ]

        for body, exchange in self.messages:
            self.messagequeue.add(body, exchange)

    def test_add(self):
        """Test add method."""
        self.assertEqual(3, models.MessagePending.objects.count())

    @patch('datawarehouse.rabbitmq.pika.BlockingConnection', mock.Mock())
    def test_send(self):
        """Test send method. Queue many messages and send them at once."""
        self.messagequeue.channel = mock.MagicMock()

        # Send all the messages.
        self.messagequeue.send()
        self.messagequeue.channel.basic_publish.assert_has_calls(
            [call(body=body, exchange=exchange, routing_key='') for body, exchange in self.messages]
        )

        # No messages pending.
        self.assertEqual(0, models.MessagePending.objects.count())

        # No messages in queue.
        self.messagequeue.channel.basic_publish.reset_mock()
        self.messagequeue.send()
        self.assertFalse(self.messagequeue.channel.basic_publish.called)

    @patch('datawarehouse.rabbitmq.pika.BlockingConnection', mock.Mock())
    def test_send_fails(self):
        # pylint: disable=protected-access
        """Test send method fails. Messages are kept in queue."""
        self.messagequeue._connect = mock.Mock()
        self.messagequeue.connection = mock.MagicMock()
        self.messagequeue.channel = mock.MagicMock()
        self.messagequeue.channel.basic_publish.side_effect = pika.exceptions.AMQPError()

        # Send all the messages. All fail.
        self.messagequeue.send()

        # All messages still pending.
        self.assertEqual(3, models.MessagePending.objects.count())

    @patch('datawarehouse.rabbitmq.pika.BlockingConnection', mock.Mock())
    def test_send_fails_middle(self):
        # pylint: disable=protected-access
        """Test send method fails while sending. Messages are kept in queue."""
        def mock_publish(**kwargs):
            """Mock publish. Fail on msg 1."""
            if kwargs['body'].endswith('1'):
                raise pika.exceptions.AMQPError()

        self.messagequeue._connect = mock.Mock()
        self.messagequeue._disconnect = mock.Mock()
        self.messagequeue.connection = mock.MagicMock()
        self.messagequeue.channel = mock.MagicMock()
        self.messagequeue.channel.basic_publish = mock_publish

        # Send all the messages. Some fail.
        self.messagequeue.send()

        # Some messages still pending.
        self.assertEqual(2, models.MessagePending.objects.count())
        self.assertTrue(self.messagequeue._disconnect.called)
