"""Test the invalid result processing."""

from datawarehouse import models, scripts

from tests import utils


class InvalidResultTestCase(utils.TestCase):
    """Unit tests for the invalid result processing."""

    recipe_id = 0

    def setUp(self):
        """Set up tests."""
        self.insertObjects([
            {'model': 'datawarehouse.project', 'fields': {'project_id': 2, 'path': 'cki-project/cki-pipeline'}},
            {'model': 'datawarehouse.pipeline', 'fields': {'pipeline_id': 1, 'project': ['cki-project/cki-pipeline'],
                                                           'created_at': '2000-01-01T01:01:01Z'}},
            {'model': 'datawarehouse.beakerresource', 'fields': {'fqdn': 'host_1'}},
            {'model': 'datawarehouse.architecture', 'fields': {'name': 'arch'}},
            {'model': 'datawarehouse.test', 'fields': {'name': 'test_1', 'fetch_url': ''}},
            {'model': 'datawarehouse.beakerstatus', 'fields': {'name': 'Aborted'}},
            {'model': 'datawarehouse.beakerstatus', 'fields': {'name': 'Cancelled'}},
            {'model': 'datawarehouse.beakerstatus', 'fields': {'name': 'Completed'}},
            {'model': 'datawarehouse.beakerresult', 'fields': {'name': 'Warn'}},
            {'model': 'datawarehouse.beakerresult', 'fields': {'name': 'Pass'}},
            {'model': 'datawarehouse.stage', 'fields': {'name': 'stage'}},
            {'model': 'datawarehouse.job', 'fields': {'name': 'job', 'stage': ['stage']}},
            {'model': 'datawarehouse.testresult', 'fields': {'name': 'PASS'}},
        ])

    def _insert_invalid_test_runs(self, states, results=None):
        self.recipe_id += 1
        if results is None:
            results = ['Pass'] * len(states)
        for task_id, (status, result) in enumerate(zip(states, results)):
            testrun = self.insertObjects([{
                'model': 'datawarehouse.testrun', 'fields': {
                    'pipeline': ['1'], 'test': ['test_1', ''], 'result': ['PASS'],  # result TODO
                    'waived': False, 'kernel_arch': ['arch']}
            }])[0]
            self.insertObjects([{
                'model': 'datawarehouse.beakertestrun', 'fields': {
                    'testrun_ptr_id': testrun.pk, 'job': ['job', 'stage'], 'jid': 1, 'beaker_resource': ['host_1'],
                    'retcode': 0, 'recipe_id': self.recipe_id, 'task_id': task_id, 'beaker_status': [status],
                    'beaker_result': [result]}
            }])

        scripts.update_invalid_results(self.recipe_id)

    def _test_results(self, states, expected):
        self._insert_invalid_test_runs(states)
        self.assertEqual(list(models.BeakerTestRun.objects
                              .filter(recipe_id=self.recipe_id)
                              .order_by('task_id')
                              .values_list('invalid_result', flat=True)), expected)

    def test_update_invalid_results_aborted(self):
        """Test that aborted recipes are processed correctly."""
        self._test_results(['Completed', 'Completed', 'Completed'], [False, False, False])
        self._test_results(['Completed', 'Completed', 'Aborted'], [False, False, False])
        self._test_results(['Completed', 'Aborted', 'Aborted'], [False, False, True])
        self._test_results(['Aborted', 'Aborted', 'Aborted'], [False, True, True])

    def test_update_invalid_results_cancelled(self):
        """Test that cancelled recipes are processed correctly."""
        self._test_results(['Completed', 'Completed', 'Completed'], [False, False, False])
        self._test_results(['Completed', 'Completed', 'Cancelled'], [False, False, False])
        self._test_results(['Completed', 'Cancelled', 'Cancelled'], [False, False, True])
        self._test_results(['Cancelled', 'Cancelled', 'Cancelled'], [False, True, True])

    def test_update_invalid_results_mixed(self):
        """Test that mixed recipes are processed correctly."""
        self._test_results(['Cancelled', 'Cancelled', 'Aborted'], [False, False, False])
        self._test_results(['Cancelled', 'Aborted', 'Aborted'], [False, False, True])
        self._test_results(['Aborted', 'Aborted', 'Cancelled'], [False, False, False])
        self._test_results(['Aborted', 'Cancelled', 'Cancelled'], [False, False, True])

    def test_view_confidence(self):
        """Test that invalid results are ignored for for the confidence view."""
        self._insert_invalid_test_runs(['Completed', 'Aborted', 'Aborted'], ['Pass', 'Warn', 'Warn'])
        for item in ('tests', 'hosts'):
            response = self.client.get(f'/confidence/{item}', {'days_ago': 'ever'})
            self.assertEqual(response.context['map'][0]['confidence'], 0.5)
            self.assertEqual(response.context['map'][0]['total'], 2)
            self.assertEqual(response.context['map'][0]['results'], {'PASS': 1, 'ERROR': 1, 'SKIP': 0, 'FAIL': 0})

    def test_model_confidence(self):
        """Test that invalid results are ignored for confidence as calculated by the models."""
        self._insert_invalid_test_runs(['Completed', 'Aborted', 'Aborted'], ['Pass', 'Warn', 'Warn'])
        self.assertEqual(models.Test.objects.first().confidence, 0.5)
        self.assertEqual(models.BeakerResource.objects.first().confidence, 0.5)

    def test_view_details(self):
        """Test that invalid results are ignored for the details view."""
        self._insert_invalid_test_runs(['Completed', 'Aborted', 'Aborted'], ['Pass', 'Warn', 'Warn'])
        for item, object_id in (('test', models.Test.objects.first().id),
                                ('host', models.BeakerResource.objects.first().id)):
            response = self.client.get(f'/details/{item}/{object_id}')
            self.assertEqual(response.context['table'][0].total_runs, 2)
            self.assertEqual(len(response.context['runs'][0]['tests']), 2)
