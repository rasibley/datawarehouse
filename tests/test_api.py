"""Test the views module."""
import json
import datetime
from freezegun import freeze_time

from django.utils import timezone
from django.contrib.auth.models import User, Permission
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from tests import utils
from datawarehouse import models, serializers


class TestAPITokenAuthentication(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Smoke test to keep token auth working."""

    def setUp(self):
        """Set Up."""
        models.Pipeline.objects.create(
            pipeline_id=1,
            project=models.Project.objects.create(project_id=0),
            gittree=models.GitTree.objects.create(name='Tree 1')
        )
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)

        permissions = Permission.objects.filter(
            codename__in=['add_issuerecord', 'change_issuerecord', 'delete_issuerecord']
        )

        test_user = User.objects.create(username='test', email='test@test.com')
        test_user.user_permissions.set(permissions)
        token = Token.objects.create(user=test_user)

        self.api_client = APIClient()
        self.api_client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def test_post(self):
        """Test POST request."""
        pipeline = models.Pipeline.objects.first()
        issue = models.Issue.objects.first()

        response = self.api_client.post(
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record',
            json.dumps({'issue_id': issue.id}), content_type="application/json")
        self.assertEqual(201, response.status_code)

    def test_delete(self):
        """Test DELETE request."""
        pipeline = models.Pipeline.objects.first()
        issue = models.Issue.objects.first()
        record = models.IssueRecord.objects.create(issue=issue, pipeline=pipeline)

        response = self.api_client.delete(
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record/{record.id}',
            content_type="application/json")
        self.assertEqual(204, response.status_code)

    def test_put(self):
        """Test PUT request."""
        pipeline = models.Pipeline.objects.first()
        issue = models.Issue.objects.first()
        issue_2 = models.Issue.objects.create(description='foo bar', ticket_url='http://other.url', kind=issue.kind)
        record = models.IssueRecord.objects.create(issue=issue, pipeline=pipeline)

        response = self.api_client.put(
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record/{record.id}',
            json.dumps({'issue_id': issue_2.id}), content_type="application/json"
        )
        self.assertEqual(204, response.status_code)


class ViewsTestCase(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Unit tests for the views module."""

    def setUp(self):
        """Set up tests."""
        self.project = models.Project.objects.create(project_id=0)
        self.git_trees = [
            models.GitTree.objects.create(name='Tree 1'),
            models.GitTree.objects.create(name='Tree 2'),
        ]
        self.pipelines = [
            models.Pipeline.objects.create(pipeline_id=1, project=self.project,
                                           gittree=self.git_trees[0], duration=1234),
            models.Pipeline.objects.create(pipeline_id=2, project=self.project,
                                           gittree=self.git_trees[1]),
        ]
        self.hosts = [
            models.BeakerResource.objects.create(id=1, fqdn='host_1'),
            models.BeakerResource.objects.create(id=2, fqdn='host_2'),
        ]
        self.kernel_arch = models.Architecture.objects.create(name='arch')
        self.tests = [
            models.Test.objects.create(id=1, name='test_1'),
            models.Test.objects.create(id=2, name='test_2'),
        ]
        self.beaker_statuses = [
            models.BeakerStatus.objects.create(name='Completed'),
            models.BeakerStatus.objects.create(name='Aborted'),
        ]
        self.beaker_results = [
            models.BeakerResult.objects.create(name='Fail'),
            models.BeakerResult.objects.create(name='Pass'),
        ]
        self.stage = models.Stage.objects.create(name='stage')
        self.jobs = [
            models.Job.objects.create(name='job_1', stage=self.stage),
            models.Job.objects.create(name='job_2', stage=self.stage),
            models.Job.objects.create(name='job_3', stage=self.stage),
            models.Job.objects.create(name='job_4', stage=self.stage),
            ]

        # pylint: disable=line-too-long
        tests_list = [
            (0, self.jobs[0], self.pipelines[0], self.tests[0], self.beaker_results[1], self.beaker_statuses[0], self.hosts[1]), # noqa
            (1, self.jobs[1], self.pipelines[0], self.tests[1], self.beaker_results[0], self.beaker_statuses[1], self.hosts[0]), # noqa
            (2, self.jobs[2], self.pipelines[1], self.tests[0], self.beaker_results[1], self.beaker_statuses[1], self.hosts[0]), # noqa
            (3, self.jobs[3], self.pipelines[1], self.tests[1], self.beaker_results[0], self.beaker_statuses[0], self.hosts[1]), # noqa
        ]

        self.test_runs = []
        for incremental_id, job, pipeline, test_, beaker_result, beaker_status, host in tests_list:
            test_run = models.BeakerTestRun.objects.create(
                job=job, jid=incremental_id, task_id=incremental_id, recipe_id=incremental_id,
                pipeline=pipeline, test=test_, beaker_result=beaker_result, beaker_status=beaker_status,
                waived=False, kernel_arch=self.kernel_arch, beaker_resource=host, retcode=0
            )
            self.test_runs.append(test_run)

        models.LintRun.objects.create(job=self.jobs[0], jid=0, command='', pipeline=self.pipelines[0], success=False)
        models.MergeRun.objects.create(job=self.jobs[1], jid=1, pipeline=self.pipelines[1], success=False)

    def test_pipeline_issue(self):
        """Test creating new issue record."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)
        pipeline = models.Pipeline.objects.first()

        # Create a issue record with no testruns.
        response = self.assert_authenticated_post(
            201, 'add_issuerecord',
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record',
            json.dumps({'issue_id': issue.id, 'testrun_ids': [], 'bot_generated': False}),
            content_type="application/json")

        self.assertEqual(
            serializers.IssueRecordSerializer(pipeline.issue_records.first()).data,
            response.json()
        )
        self.assertEqual(1, pipeline.issue_records.count())

        issue_record = pipeline.issue_records.first()
        self.assertEqual(issue, issue_record.issue)
        self.assertEqual([], list(issue_record.testruns.all()))
        self.assertFalse(issue_record.bot_generated)

        # Create a issue record with some testruns.
        self.assert_authenticated_post(
            201, 'add_issuerecord',
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record',
            json.dumps({
                'issue_id': issue.id,
                'testrun_ids': list(pipeline.test_jobs.values_list('id', flat=True)[:2]),
                'bot_generated': True,
            }),
            content_type="application/json")

        self.assertEqual(2, pipeline.issue_records.count())

        issue_record = pipeline.issue_records.first()
        self.assertEqual(issue, issue_record.issue)
        self.assertEqual(
            list(pipeline.test_jobs.all())[:2],
            list(issue_record.testruns.all()))
        self.assertTrue(issue_record.bot_generated)

    def test_pipeline_issue_delete(self):
        """Test deleting issue record."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)
        pipeline = models.Pipeline.objects.first()

        # Create a issue record with no testruns.
        self.assert_authenticated_post(
            201, 'add_issuerecord',
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record',
            json.dumps({'issue_id': issue.id, 'testrun_ids': [], 'bot_generated': False}),
            content_type="application/json")

        self.assertEqual(1, pipeline.issue_records.count())

        # Delete it.
        record_id = pipeline.issue_records.first().id
        self.assert_authenticated_delete(
            204, 'delete_issuerecord',
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record/{record_id}',
            content_type="application/json")

        self.assertEqual(0, pipeline.issue_records.count())

    def test_pipeline_issue_put(self):
        """Test editing issue record."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue_1 = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)
        issue_2 = models.Issue.objects.create(description='foo foo', ticket_url='http://other.url', kind=issue_kind)
        pipeline = models.Pipeline.objects.first()

        # Create a issue record with some testruns.
        self.assert_authenticated_post(
            201, 'add_issuerecord',
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record',
            json.dumps({
                'issue_id': issue_1.id,
                'testrun_ids': list(pipeline.test_jobs.values_list('id', flat=True)[:2]),
                'bot_generated': True,
            }),
            content_type="application/json")

        issue_record = pipeline.issue_records.first()
        self.assertEqual(
            list(pipeline.test_jobs.all())[:2],
            list(issue_record.testruns.all())
        )

        # Change issue kind.
        self.assert_authenticated_put(
            204, 'change_issuerecord',
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record/{issue_record.id}',
            json.dumps({'issue_id': issue_2.id}), content_type="application/json"
        )

        issue_record = pipeline.issue_records.first()
        self.assertEqual(issue_2, issue_record.issue)

        # Change bot_generated.
        self.assert_authenticated_put(
            204, 'change_issuerecord',
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record/{issue_record.id}',
            json.dumps({'bot_generated': False}), content_type="application/json"
        )

        issue_record = pipeline.issue_records.first()
        self.assertFalse(issue_record.bot_generated)

        # Change testruns.
        self.assert_authenticated_put(
            204, 'change_issuerecord',
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record/{issue_record.id}',
            json.dumps({
                'testrun_ids': list(pipeline.test_jobs.values_list('id', flat=True)[:1]),
            }),
            content_type="application/json"
        )

        issue_record = pipeline.issue_records.first()
        self.assertEqual(
            list(pipeline.test_jobs.all())[:1],
            list(issue_record.testruns.all())
        )

        # No testruns at all.
        self.assert_authenticated_put(
            204, 'change_issuerecord',
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record/{issue_record.id}',
            json.dumps({
                'testrun_ids': [],
            }),
            content_type="application/json"
        )

        issue_record = pipeline.issue_records.first()
        self.assertEqual(
            [],
            list(issue_record.testruns.all())
        )

        # Change all at once.
        self.assert_authenticated_put(
            204, 'change_issuerecord',
            f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record/{issue_record.id}',
            json.dumps({
                'issue_id': issue_1.id,
                'testrun_ids': list(pipeline.test_jobs.values_list('id', flat=True)[:2]),
                'bot_generated': True,
            }),
            content_type="application/json"
        )

        issue_record = pipeline.issue_records.first()
        self.assertEqual(issue_1, issue_record.issue)
        self.assertTrue(issue_record.bot_generated)
        self.assertEqual(
            list(pipeline.test_jobs.all())[:2],
            list(issue_record.testruns.all())
        )

    def test_pipeline_issue_get_single(self):
        """Test getting issue record."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)
        pipeline = models.Pipeline.objects.first()

        record = pipeline.issue_records.create(issue_id=issue.id)

        response = self.client.get(f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record/{record.id}')
        self.assertEqual(200, response.status_code)

        self.assertEqual(
            serializers.IssueRecordSerializer(record).data,
            response.json()
        )

    def test_pipeline_issue_get_all(self):
        """Test getting issue record."""
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)
        pipeline = models.Pipeline.objects.first()

        pipeline.issue_records.create(issue_id=issue.id)
        pipeline.issue_records.create(issue_id=issue.id)

        response = self.client.get(f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record')
        self.assertEqual(200, response.status_code)

        self.assertEqual(
            {
                'issue_records': serializers.IssueRecordSerializer(pipeline.issue_records.all(), many=True).data
            },
            response.json()['results']
        )

    def test_pipeline_issue_get_all_filtered(self):
        """Test getting issue record filtering by kind_id."""
        issue_1 = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url',
                                              kind=models.IssueKind.objects.create(description="fail 1", tag="1"))
        issue_2 = models.Issue.objects.create(description='bar foo', ticket_url='http://other.url',
                                              kind=models.IssueKind.objects.create(description="fail 2", tag="2"))

        pipeline = models.Pipeline.objects.first()

        pipeline.issue_records.create(issue_id=issue_1.id)
        pipeline.issue_records.create(issue_id=issue_2.id)

        response = self.client.get(f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record?kind_id={issue_2.kind.id}')
        self.assertEqual(200, response.status_code)

        self.assertEqual(
            {
                'issue_records': serializers.IssueRecordSerializer(
                    pipeline.issue_records.filter(issue__kind=issue_2.kind), many=True).data
            },
            response.json()['results']
        )

        response = self.client.get(f'/api/1/pipeline/{pipeline.pipeline_id}/issue/record?issue_id={issue_2.id}')
        self.assertEqual(200, response.status_code)

        self.assertEqual(
            {
                'issue_records': serializers.IssueRecordSerializer(
                    issue_2.issue_records.all(), many=True).data
            },
            response.json()['results']
        )

    def test_pipeline_get_json_not_found(self):
        """Test get_pipeline json. Pipeline not found."""
        response = self.client.get(f'/api/1/pipeline/0')
        self.assertEqual(404, response.status_code)
        self.assertEqual(b'', response.content)

    def test_pipeline_get_json(self):
        """Test get_pipeline json."""
        pipeline = models.Pipeline.objects.first()

        response = self.client.get(f'/api/1/pipeline/{pipeline.pipeline_id}')
        self.assertEqual(200, response.status_code)

        self.assertEqual(
            serializers.PipelineSerializer(pipeline).data,
            response.json()
        )

    def test_pipeline_get_json_reporter(self):
        """Test get_pipeline json. Reporter style."""
        pipeline = models.Pipeline.objects.first()

        response = self.client.get(f'/api/1/pipeline/{pipeline.pipeline_id}?style=reporter')
        self.assertEqual(200, response.status_code)

        self.assertEqual(
            serializers.PipelineOrderedSerializer(pipeline).data,
            response.json()
        )

    def test_failures_all(self):
        """Test failures all stages."""
        response = self.client.get('/api/1/pipeline/failures/all')
        self.assertEqual(
            response.json()['results'],
            {
                'stage': 'all',
                'pipelines': serializers.PipelineSerializer(
                    models.Pipeline.objects.exclude(duration=None), many=True).data,
            }
        )

    def test_failures_lint(self):
        """Test failures lint."""
        response = self.client.get('/api/1/pipeline/failures/lint')
        self.assertEqual(
            response.json()['results'],
            {
                'stage': 'lint',
                'pipelines': serializers.PipelineSerializer([self.pipelines[0]], many=True).data,
            }
        )

    def test_failures_lint_jobs(self):
        """Test getting jobs that failed lint."""
        pipeline = self.pipelines[0]
        pipeline.lint_jobs.create(job=self.jobs[0], jid=1, command="lint", success=True)
        self.assertTrue(pipeline.lint_jobs.filter(success=True).exists())
        self.assertTrue(pipeline.lint_jobs.filter(success=False).exists())

        response = self.client.get(f'/api/1/pipeline/{pipeline.pipeline_id}/jobs/lint/failures')
        self.assertEqual(
            response.json()['results'],
            {
                'jobs': serializers.LintRunSerializer(pipeline.lint_jobs.filter(success=False), many=True).data,
            }
        )

    def test_failures_test_jobs(self):
        """Test getting jobs that failed test."""
        pipeline = self.pipelines[0]
        self.assertTrue(pipeline.test_jobs.filter(passed=True).exists())
        self.assertTrue(pipeline.test_jobs.filter(passed=False, untrusted=False).exists())

        response = self.client.get(f'/api/1/pipeline/{pipeline.pipeline_id}/jobs/test/failures')
        self.assertEqual(
            response.json()['results'],
            {
                'jobs': serializers.TestRunSerializer(
                    pipeline.test_jobs.filter(passed=False, untrusted=False), many=True
                ).data,
            }
        )

    def test_get_test(self):
        """Test get all tests."""
        response = self.client.get('/api/1/test')
        self.assertEqual(
            response.json()['results'],
            {
                'tests': serializers.TestSerializer(models.Test.objects.all(), many=True).data
            }
        )

    def test_get_single_test(self):
        """Test get single tests."""
        response = self.client.get('/api/1/test/1')
        self.assertEqual(
            response.json(),
            serializers.TestSerializer(models.Test.objects.get(id=1)).data
        )

    def test_get_single_test_404(self):
        """Test get single tests. It doesn't exist."""
        response = self.client.get('/api/1/test/1234')
        self.assertEqual(404, response.status_code)
        self.assertEqual(b'', response.content)

    @staticmethod
    def _configure_test_issue_records():
        """Configure issue records for the test."""
        test = models.Test.objects.first()
        testrun = test.testrun_set.first()

        # Create a issue for that test.
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)
        issue_record = models.IssueRecord.objects.create(issue=issue, pipeline=testrun.pipeline)
        issue_record.testruns.add(testrun)

        # Set up pipeline's created_at to now-1h
        now = timezone.now()
        testrun.pipeline.created_at = now - datetime.timedelta(hours=1)
        testrun.pipeline.save()

        return test

    def test_get_test_issue_records(self):
        """Test get a test issue records."""
        test = self._configure_test_issue_records()

        response = self.client.get(f'/api/1/test/{test.id}/issue/record')
        self.assertEqual(
            response.json()['results'],
            {
                'issues': serializers.IssueRecordSerializer(
                    models.IssueRecord.objects.filter(testruns__test=test),
                    many=True
                ).data
            }
        )

    def test_get_test_issue_records_since_2_hrs(self):
        """Test get a test issue records. Since 2 hours ago."""
        test = self._configure_test_issue_records()
        since_2_hours = datetime.datetime.now() - datetime.timedelta(hours=2)

        response = self.client.get(f'/api/1/test/{test.id}/issue/record?since={since_2_hours}')
        self.assertEqual(
            response.json()['results'],
            {
                'issues': serializers.IssueRecordSerializer(
                    models.IssueRecord.objects.filter(testruns__test=test),
                    many=True
                ).data
            }
        )

    def test_get_test_issue_records_since_now(self):
        """Test get a test issue records. Since now."""
        test = self._configure_test_issue_records()
        now = datetime.datetime.now()

        response = self.client.get(f'/api/1/test/{test.id}/issue/record?since={now}')
        self.assertEqual(
            response.json()['results'],
            {
                'issues': []
            }
        )

    def test_get_records_for_issue(self):
        """Test get records of an issue."""
        self._configure_test_issue_records()
        issue = models.Issue.objects.get(ticket_url='http://some.url')

        response = self.client.get(f'/api/1/issue/{issue.id}/record')
        self.assertEqual(
            response.json()['results'],
            {
                'records': serializers.IssueRecordSerializer(
                    models.Issue.objects.get(id=issue.id).issue_records.all(),
                    many=True
                ).data
            }
        )

    def test_get_records_for_issue_404(self):
        """Test get records of an issue that doesn't exist."""
        response = self.client.get('/api/1/issue/1234/record')
        self.assertEqual(404, response.status_code)
        self.assertEqual(b'', response.content)

    def test_get_test_stats(self):
        """Test get stats of a test."""
        self._configure_test_issue_records()

        response = self.client.get(f'/api/1/test/1/stats')
        self.assertEqual(
            response.json(),
            {
                'test': {'fetch_url': None, 'id': 1, 'maintainers': [], 'name': 'test_1', 'universal_id': None},
                'issues': {'1': {'rate': 0.5, 'reports': 1}},
                'total_failures': 0,
                'total_runs': 2
            }
        )

    def test_get_test_stats_404(self):
        """Test get stats of a test."""
        response = self.client.get(f'/api/1/test/1234/stats')
        self.assertEqual(404, response.status_code)
        self.assertEqual(b'', response.content)

    def test_get_issue_regex(self):
        """Test get issue regex."""
        self._configure_test_issue_records()
        issue = models.Issue.objects.first()

        models.IssueRegex.objects.create(
            text_match='^([a-zA-Z_]+)+$',
            file_name_match='^([a-zA-Z_]+)+$',
            test_name_match='^([a-zA-Z_]+)+$',
            issue=issue,
        )

        response = self.client.get(f'/api/1/issue/regex')
        self.assertEqual(
            response.json(),
            {
                'count': 1,
                'next': None,
                'previous': None,
                'results': {
                    'issue_regexes': [
                        {
                            'id': 1,
                            'text_match':  '^([a-zA-Z_]+)+$',
                            'file_name_match':  '^([a-zA-Z_]+)+$',
                            'test_name_match':  '^([a-zA-Z_]+)+$',
                            'issue': serializers.IssueSerializer(issue).data
                        }
                    ]
                }
            }
        )


class TestPipelineActions(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Test Pipeline Action methods."""

    def setUp(self):
        """Set Up."""
        models.Pipeline.objects.create(
            pipeline_id=1,
            project=models.Project.objects.create(project_id=0),
            gittree=models.GitTree.objects.create(name='Tree 1')
        )

    def test_create(self):
        """Test create actions."""
        pipeline = models.Pipeline.objects.first()

        response = self.assert_authenticated_post(
            201, 'add_action',
            f'/api/1/pipeline/{pipeline.pipeline_id}/action',
            json.dumps({'name': 'did_stuff'}), content_type="application/json")

        self.assertEqual(
            serializers.ActionSerializer(pipeline.actions.first()).data,
            response.json()
        )

        self.assertEqual(1, pipeline.actions.count())

    def test_create_bad_name(self):
        """Test create actions with a non compliant name."""
        pipeline = models.Pipeline.objects.first()

        self.assert_authenticated_post(
            400, 'add_action',
            f'/api/1/pipeline/{pipeline.pipeline_id}/action',
            json.dumps({'name': 'did stuff'}), content_type="application/json")

        self.assert_authenticated_post(
            400, 'add_action',
            f'/api/1/pipeline/{pipeline.pipeline_id}/action',
            json.dumps({'name': 'did-stuff'}), content_type="application/json")

    def test_get_actions(self):
        """Test get actions."""
        pipeline = models.Pipeline.objects.first()

        with freeze_time("2010-01-02 09:00:00"):
            pipeline.actions.create(
                kind=models.ActionKind.objects.create(name='triaged')
            )

        with freeze_time("2010-01-02 09:01:00"):
            pipeline.actions.create(
                kind=models.ActionKind.objects.create(name='reported')
            )

        response = self.client.get(f'/api/1/pipeline/{pipeline.pipeline_id}/action')
        self.assertEqual(
            response.json(),
            {
                'count': 2,
                'next': None,
                'previous': None,
                'results': {
                    'actions': [
                        {'created_at': '2010-01-02T09:00:00Z',
                         'kind': {'name': 'triaged'}},
                        {'created_at': '2010-01-02T09:01:00Z',
                         'kind': {'name': 'reported'}},
                    ]
                }
            }
        )


class TestIssueAPI(utils.TestCase):
    # pylint: disable=too-many-instance-attributes, too-many-public-methods
    """Tests for the Issue endpoints."""

    def test_list_issues(self):
        """Test list issues."""
        models.Pipeline.objects.create(
            pipeline_id=1,
            project=models.Project.objects.create(project_id=0),
            gittree=models.GitTree.objects.create(name='Tree 1')
        )
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)
        models.Issue.objects.create(description='other foo bar', ticket_url='http://other.url', kind=issue_kind)

        response = self.client.get(f'/api/1/issue')

        self.assertEqual(
            response.json()['results'],
            {'issues': serializers.IssueSerializer(models.Issue.objects.all(), many=True).data},
        )

    def test_get_issue(self):
        """Test get issue."""
        models.Pipeline.objects.create(
            pipeline_id=1,
            project=models.Project.objects.create(project_id=0),
            gittree=models.GitTree.objects.create(name='Tree 1')
        )
        issue_kind = models.IssueKind.objects.create(description="fail 1", tag="1")
        issue = models.Issue.objects.create(description='foo bar', ticket_url='http://some.url', kind=issue_kind)

        response = self.client.get(f'/api/1/issue/{issue.id}')

        self.assertEqual(
            response.json(),
            serializers.IssueSerializer(issue).data,
        )
