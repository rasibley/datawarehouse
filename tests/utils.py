"""TestCase with utils."""
import os
import json

from django import test
from django.db.models.query import QuerySet
from django.core import serializers
from django.contrib.auth.models import User, Permission
from django.test import Client

from datawarehouse import models


ASSETS_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'assets')


class TestCase(test.TestCase):
    # pylint: disable=invalid-name
    """TestCase class."""

    def __init__(self, *args, **kwargs):
        """Override Init."""
        super(TestCase, self).__init__(*args, **kwargs)
        self.allow_inheritance = True

    def _resolve_querysets(self, data):
        """Resolve QuerySets."""
        if isinstance(data, dict):
            for key, value in data.items():
                data[key] = self._resolve_querysets(value)
        elif isinstance(data, list):
            for key, value in enumerate(data):
                data[key] = self._resolve_querysets(value)
        elif isinstance(data, QuerySet):
            data = list(data)
        elif hasattr(data, 'object_list'):
            data = self._resolve_querysets(data.object_list)

        if self.allow_inheritance:
            data = self.get_parent_class(data)

        return data

    def assertContextEqual(self, first, second, allow_inheritance=True):
        """
        Assert Context items are equal.

        Second can contain only some of the keys from first.
        """
        self.allow_inheritance = allow_inheritance
        for key in second.keys():
            self.assertEqual(self._resolve_querysets(first[key]), self._resolve_querysets(second[key]), key)

    @staticmethod
    def insertObjects(objects):
        """
        Insert objects into the database.

        Objects are formatted as returned by dumpdata --format json.
        """
        result = []
        for m in serializers.deserialize('json', json.dumps(list(objects))):
            m.save()
            result.append(m.object)
        return result

    @staticmethod
    def get_parent_class(obj):
        """Return the parent class of the object."""
        if isinstance(obj, models.BeakerTestRun):
            return obj.testrun_ptr

        return obj

    @staticmethod
    def get_authenticated_client():
        """Create and return an authenticated client instance."""
        try:
            User.objects.get(username='test')
        except User.DoesNotExist:
            User.objects.create_user(username='test', password='pass')

        client = Client()
        client.login(username='test', password='pass')

        return client

    def assert_authenticated_post(self, response_code, permission_codename, *args, **kwargs):
        """Assert authenticated POST."""
        return self.assert_authenticated('post', response_code, permission_codename, *args, **kwargs)

    def assert_authenticated_delete(self, response_code, permission_codename, *args, **kwargs):
        """Assert authenticated DELETE."""
        return self.assert_authenticated('delete', response_code, permission_codename, *args, **kwargs)

    def assert_authenticated_put(self, response_code, permission_codename, *args, **kwargs):
        """Assert authenticated PUT."""
        return self.assert_authenticated('put', response_code, permission_codename, *args, **kwargs)

    def assert_authenticated(self, method, response_code, permission_codename, *args, **kwargs):
        """
        Test an authenticated endpoint.

        Ensure the request is forbidden without the correct permissions.
        """
        auth_client = self.get_authenticated_client()
        user = User.objects.get(username='test')
        permission = Permission.objects.get(codename=permission_codename)

        method_anon = getattr(self.client, method)
        method_auth = getattr(auth_client, method)

        # Test anonymous request.
        response = method_anon(*args, **kwargs)
        self.assertIn(response.status_code, [400, 401, 403])

        # Authenticated request without permission.
        user.user_permissions.remove(permission)
        response = method_auth(*args, **kwargs)
        self.assertIn(response.status_code, [400, 401, 403])

        # Authenticated request with permission.
        user.user_permissions.add(permission)
        response = method_auth(*args, **kwargs)
        self.assertEqual(response_code, response.status_code)
        user.user_permissions.remove(permission)

        return response
