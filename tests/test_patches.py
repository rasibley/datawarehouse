"""Test the patches module."""

import json
import os

from django import test
import responses

from datawarehouse import models, patches, serializers


TESTS_DIR = os.path.dirname(os.path.abspath(__file__))


def mock_series(file='patchwork-series.json'):
    """Return mock series."""
    patches.PATCHWORK_API.url = 'http://server/api'
    mock_patchwork = os.path.join(TESTS_DIR, file)
    with open(mock_patchwork, 'r') as series_file:
        series = json.load(series_file)
    responses.add(responses.GET, url=series['patches'][0]['url'][:-1],  # get rid of trailing slash
                  json={'series': [{'id': series['id']}]})
    responses.add(responses.GET, url=series['url'][:-1],  # get rid of trailing slash
                  json=series)
    return series


class PatchesTestCase(test.TestCase):
    """Unit tests for the patches module."""

    @responses.activate
    def test_store_patchwork_series(self):
        """Test that patchwork projects are successfully imported."""
        series = mock_series()

        patches.store_patchwork_series(series)
        self._verify_patchwork(series)

        patches.store_patchwork_series(series)
        self._verify_patchwork(series)

    @responses.activate
    def test_store_patchwork_series_patches(self):
        """Test that patches are selectively returned."""
        series = mock_series()

        patch_indices = (0, 1)
        patch_urls = [series['patches'][i]['mbox'] for i in patch_indices]
        selected_patches = patches.store_patchwork_series_patches(patch_urls)
        self._verify_patchwork(series, patch_urls=patch_urls, selected_patches=selected_patches)

    @responses.activate
    def test_skipped(self):
        """Test that series are correctly marked as skipped."""
        series = mock_series(file='patchwork-series-skipped.json')

        patches.store_patchwork_series(series)
        self._verify_patchwork(series)

        series = models.PatchworkSeries.objects.get()
        self.assertTrue(series.skipped)

    def _verify_patchwork(self, series, patch_urls=(), selected_patches=()):
        exp_submitter = {
            'name': series['submitter']['name'],
            'email': series['submitter']['email'],
            'submitter_id': series['submitter']['id'],
        }

        exp_project = {
            'name': series['project']['link_name'],
            'project_id': series['project']['id'],
        }

        exp_series = {
            'series_id': series['id'],
            'url': series['url'],
            'web_url': series['web_url'],
            'name': series['name'],
            'submitter': exp_submitter,
            'project': exp_project,
            'submitted_date': series['date'] + 'Z',
        }

        exp_patches = [{
            'url': p['url'],
            'web_url': p['web_url'],
            'subject': p['name'],
            'series': [exp_series],
            'msgid': p['msgid'],
            'mbox': p['mbox'],
            'patch_id': p['id'],
        } for p in series['patches']]

        db_submitters = models.PatchworkSubmitter.objects.all()
        self.assertEqual(serializers.PatchworkSubmitterSerializer(db_submitters, many=True).data, [exp_submitter])

        db_projects = models.PatchworkProject.objects.all()
        self.assertEqual(serializers.PatchworkProjectSerializer(db_projects, many=True).data, [exp_project])

        db_seriess = models.PatchworkSeries.objects.all()
        self.assertEqual(serializers.PatchworkSeriesSerializer(db_seriess, many=True).data, [exp_series])

        db_patches = models.PatchworkPatch.objects.all()
        self.assertEqual(serializers.PatchworkPatchSerializer(db_patches, many=True).data, exp_patches)

        for index, patch_url in enumerate(patch_urls):
            exp_patch = next(p for p in exp_patches if p['mbox'] == patch_url)
            self.assertEqual(serializers.PatchworkPatchSerializer(selected_patches[index]).data, exp_patch)
