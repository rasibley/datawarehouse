"""Test the beaker module."""
import os
import json
import responses
from django import test

from datawarehouse.beaker import Beaker


TESTS_DIR = os.path.dirname(os.path.abspath(__file__))


class BeakerTestCase(test.TestCase):
    """Unit tests for the beaker module."""

    def test_filter_tasks_from_recipe(self):
        """Test that get_recipe_tasks returns the correct tasks."""
        recipe = {
            'tasks': [
                {'name': 'some name we don\'t care',
                 'params': [{'id': 176525027, 'name': 'CKI_CASE_NAME', 'value': 'this name is important'}]},
                {'name': 'some name we don\'t care',
                 'params': [{'id': 176525028, 'name': 'BLA BLA', 'value': 'this is ignored'}]},
                {'name': 'some name we don\'t care',
                 'params': [{'id': 176525029, 'name': 'CKI_NAME', 'value': 'this name is important'}]},
            ]
        }
        result = Beaker.filter_tasks_from_recipe(recipe)

        self.assertEqual(
            [recipe['tasks'][0], recipe['tasks'][2]],
            result
        )

    @responses.activate
    def test_get_recipeset_tasks(self):
        """Integration test for get_recipeset_tasks."""
        with open(os.path.join(TESTS_DIR, 'beaker.json')) as json_file:
            json_input = json.loads(json_file.read())

        responses.add(responses.GET, url=f'https://beaker/recipesets/1', json=json_input)
        beaker = Beaker('https://beaker')
        output = beaker.get_recipeset_tasks('r:1')

        expected_output = {
            7924748: [
                {'name': 'Boot test',
                 'result': 'Pass',
                 'status': 'Completed',
                 'waived': False,
                 'maintainers': [],
                 'task_id': 'T:106416790',
                 'beaker_resource_fqdn': 'redacted.redhat.com',
                 'start_time': '2020-02-19 13:16:17',
                 'finish_time': '2020-02-19 13:21:25',
                 'fetch_url': 'https://github.com/CKI-project/tests-beaker/archive/' +
                              'master.zip#distribution/kpkginstall',
                 'cki_name': 'Boot test',
                 'universal_id': None},
                {'name': 'selinux-policy: serge-testsuite',
                 'result': 'Pass',
                 'status': 'Completed',
                 'waived': False,
                 'maintainers': ['Redacted <redacted@redhat.com>',
                                 'Redacted <redacted@redhat.com>'],
                 'task_id': 'T:106416791',
                 'beaker_resource_fqdn': 'redacted.redhat.com',
                 'start_time': '2020-02-19 13:21:26',
                 'finish_time': '2020-02-19 13:25:28',
                 'fetch_url': 'https://github.com/CKI-project/tests-beaker/archive/' +
                              'master.zip#packages/selinux-policy/serge-testsuite',
                 'cki_name': 'selinux-policy: serge-testsuite',
                 'universal_id': 'selinux'},
                {'name': 'stress: stress-ng',
                 'result': 'Pass',
                 'status': 'Completed',
                 'waived': False,
                 'maintainers': ['Redacted <redacted@redhat.com>'],
                 'task_id': 'T:106416792',
                 'beaker_resource_fqdn': 'redacted.redhat.com',
                 'start_time': '2020-02-19 13:25:28',
                 'finish_time': '2020-02-19 13:50:04',
                 'fetch_url': 'https://github.com/CKI-project/tests-beaker/archive/' +
                              'master.zip#stress/stress-ng',
                 'cki_name': 'stress: stress-ng',
                 'universal_id': None},
                {'name': '/kernel/livepatch/kselftests',
                 'result': 'Pass',
                 'status': 'Completed',
                 'waived': True,
                 'maintainers': ['Redacted <redacted@redhat.com>',
                                 'Redacted <redacted@redhat.com>'],
                 'task_id': 'T:106416793',
                 'beaker_resource_fqdn': 'redacted.redhat.com',
                 'start_time': '2020-02-19 13:50:04',
                 'finish_time': '2020-02-19 14:38:45',
                 'fetch_url': None,
                 'cki_name': 'Livepatch kselftests',
                 'universal_id': None}
            ]
        }

        self.assertEqual(expected_output, output)
