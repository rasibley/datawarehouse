#! /bin/bash
python3 manage.py migrate
python3 manage.py collectstatic
if [ -z "${IS_PRODUCTION:-}" ]; then
    python3 manage.py runserver 0.0.0.0:8000
else
    supervisord
    tail --follow=name --retry /var/log/nginx/dw-{access,error}.log
fi
