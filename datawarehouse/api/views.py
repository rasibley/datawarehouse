"""API Views."""
import re
import mailbox
import datetime
from distutils.util import strtobool

import email.utils
from django.db.models import Q
from django.utils import timezone
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from datawarehouse import helpers, pagination, serializers, models, utils, signals


class PipelineIssueRecord(APIView):
    """PipelineIssueRecord class."""

    queryset = models.IssueRecord.objects.none()

    def _get(self, request, pipeline_id, record_id):
        issue_record = models.IssueRecord.objects.get(
            pipeline__pipeline_id=pipeline_id,
            id=record_id,
        )

        serialized_issue_record = serializers.IssueRecordSerializer(issue_record).data

        return Response(serialized_issue_record)

    def _list(self, request, pipeline_id):
        kind_id = request.GET.get('kind_id')
        issue_id = request.GET.get('issue_id')

        issue_records = models.IssueRecord.objects.filter(
            pipeline__pipeline_id=pipeline_id
        )

        if kind_id:
            issue_records = issue_records.filter(
                issue__kind__id=kind_id
            )

        if issue_id:
            issue_records = issue_records.filter(
                issue__id=issue_id
            )

        paginator = pagination.DatawarehousePagination()
        paginated_issue_records = paginator.paginate_queryset(issue_records, request)

        serialized_issue_records = serializers.IssueRecordSerializer(paginated_issue_records, many=True).data

        context = {
            'issue_records': serialized_issue_records,
        }

        return paginator.get_paginated_response(context)

    def get(self, request, pipeline_id, record_id=None):
        if record_id:
            return self._get(request, pipeline_id, record_id)
        else:
            return self._list(request, pipeline_id)

    def post(self, request, pipeline_id):
        """Mark a pipeline's issue."""
        issue_id = request.data.get('issue_id')
        testrun_ids = request.data.get('testrun_ids', [])
        bot_generated = request.data.get('bot_generated', False)

        issue_record = helpers.create_issue_record(issue_id, pipeline_id, testrun_ids, bot_generated)
        serialized_issue_record = serializers.IssueRecordSerializer(issue_record).data

        return Response(serialized_issue_record, status=status.HTTP_201_CREATED)

    def delete(self, request, pipeline_id, record_id):
        """Delete IssueRecord from Pipeline."""
        try:
            issue_record = models.IssueRecord.objects.get(
                pipeline__pipeline_id=pipeline_id,
                id=record_id
            )
        except models.IssueRecord.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        issue_record.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, pipeline_id, record_id):
        """Edit testruns from IssueRecord."""
        issue_id = request.data.get('issue_id')
        testrun_ids = request.data.get('testrun_ids')
        bot_generated = request.data.get('bot_generated')

        try:
            issue_record = models.IssueRecord.objects.get(
                pipeline__pipeline_id=pipeline_id,
                id=record_id
            )
        except models.IssueRecord.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if issue_id is not None:
            issue_record.issue = models.Issue.objects.get(id=issue_id)

        if bot_generated is not None:
            issue_record.bot_generated = bot_generated

        issue_record.save()

        if testrun_ids is not None:
            testruns = models.TestRun.objects.filter(
                pipeline__pipeline_id=pipeline_id,
                id__in=testrun_ids
            )
            issue_record.testruns.set(testruns)

        return Response(status=status.HTTP_204_NO_CONTENT)


class PipelineByFailures(APIView):
    """Endpoint for pipeline failure handling."""

    queryset = models.Pipeline.objects.none()

    def get(self, request, stage_name):
        """Get a list of failed pipelines."""
        issues = helpers.get_failed_pipelines(stage_name).filter(issue_records=None).exclude(duration=None)

        paginator = pagination.DatawarehousePagination()
        paginated_pipelines = paginator.paginate_queryset(issues, request)

        serialized_pipelines = serializers.PipelineSerializer(paginated_pipelines, many=True).data

        context = {
            'pipelines': serialized_pipelines,
            'stage': stage_name
        }

        return paginator.get_paginated_response(context)


class IssueRecords(APIView):
    """Endpoint for Issue Records handling."""

    queryset = models.IssueRecord.objects.none()

    def get(self, request, issue_id):
        """Get a list of Issue Records."""
        try:
            issue = models.Issue.objects.get(id=issue_id)
        except models.Issue.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        records = models.IssueRecord.objects.filter(issue=issue)

        paginator = pagination.DatawarehousePagination()
        paginated_records = paginator.paginate_queryset(records, request)

        serialized_records = serializers.IssueRecordSerializer(paginated_records, many=True).data

        context = {
            'records': serialized_records,
        }

        return paginator.get_paginated_response(context)


class Issue(APIView):
    """Endpoint for Issue handling."""

    queryset = models.Issue.objects.none()

    def get(self, request, issue_id=None):
        """GET request."""

        if issue_id:
            return self._get(request, issue_id)

        return self._list(request)

    def _get(self, request, issue_id):
        """Get a single Issue."""
        try:
            issue = models.Issue.objects.get(id=issue_id)
        except models.Issue.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serialized_issue = serializers.IssueSerializer(issue).data
        return Response(serialized_issue)

    def _list(self, request):
        """Get a list of Issues. Optionally filter by resolved status."""
        resolved = request.GET.get('resolved')

        issues = models.Issue.objects.all()

        if resolved:
            issues = issues.filter(resolved=strtobool(resolved))

        paginator = pagination.DatawarehousePagination()
        paginated_issues = paginator.paginate_queryset(issues, request)
        serialized_issues = serializers.IssueSerializer(paginated_issues, many=True).data

        context = {
            'issues': serialized_issues,
        }

        return paginator.get_paginated_response(context)


class Pipeline(APIView):
    """Get pipeline data."""

    queryset = models.Pipeline.objects.none()

    def get(self, request, pipeline_id):
        """Get information of a pipeline."""
        style = request.GET.get('style')

        try:
            pipe = models.Pipeline.objects.get(pipeline_id=pipeline_id)
        except models.Pipeline.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if style == 'reporter':
            serializer = serializers.PipelineOrderedSerializer
        else:
            serializer = serializers.PipelineSerializer

        return Response(serializer(pipe, context={'request': request}).data)


class PipelineReport(APIView):
    """Reports and pipelines."""

    queryset = models.Report.objects.none()

    def get(self, request, pipeline_id):
        """Get the reports of a pipeline."""
        try:
            pipeline = models.Pipeline.objects.get(pipeline_id=pipeline_id)
        except models.Pipeline.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        paginator = pagination.DatawarehousePagination()
        paginated_reports = paginator.paginate_queryset(
            pipeline.reports.all(),
            request)

        context = {
            'reports': serializers.ReportSerializer(paginated_reports, many=True).data,
        }

        return paginator.get_paginated_response(context)

    def post(self, request, pipeline_id):
        """Submit a new report for the pipeline."""
        content = request.data.get('content')
        mbox = mailbox.mboxMessage(content)

        addr_to = mbox.get_all('to', [])
        addr_cc = mbox.get_all('cc', [])
        subject = mbox.get('Subject')
        date = mbox.get('Date')
        msgid = mbox.get('Message-ID')

        if models.Report.objects.filter(msgid=msgid).exists():
            return Response("MsgID already exists.", status=status.HTTP_400_BAD_REQUEST)

        if mbox.is_multipart():
            for part in mbox.walk():
                ctype = part.get_content_type()
                cdispo = str(part.get('Content-Disposition'))

                if ctype == 'text/plain' and 'attachment' not in cdispo:
                    body = part.get_payload(decode=True).decode()
                    break
        else:
            body = mbox.get_payload(decode=True).decode()

        pipeline = models.Pipeline.objects.get(pipeline_id=pipeline_id)

        sent_at = email.utils.parsedate_to_datetime(date)
        try:
            sent_at = timezone.make_aware(sent_at)
        except ValueError:
            # tzinfo is already set
            pass

        report = models.Report.objects.create(
            pipeline=pipeline,
            subject=subject,
            body=body,
            msgid=msgid,
            sent_at=sent_at,
            raw=content,
        )

        for _, addr in email.utils.getaddresses(addr_to):
            recipient, _ = models.Recipient.objects.get_or_create(email=addr)
            report.addr_to.add(recipient)

        for _, addr in email.utils.getaddresses(addr_cc):
            recipient, _ = models.Recipient.objects.get_or_create(email=addr)
            report.addr_cc.add(recipient)

        serialized_report = serializers.ReportSerializer(report).data

        return Response(serialized_report, status=status.HTTP_201_CREATED)


class ReportMissing(APIView):
    """Get pipeline without report sent."""

    queryset = models.Report.objects.none()

    def get(self, request):
        """Get list of pipelines without reports."""
        since = request.GET.get('since')

        # Exclude newer than 3 hours as grace time.
        # gitlab.cee.redhat.com/cki-project/deployment/datawarehouse-report-crawler
        last_3_hours = timezone.now() - datetime.timedelta(hours=3)

        pipelines = (models.Pipeline.objects.add_stats()
                     .filter(stats_report_exists=False)
                     .exclude(Q(duration=None) | Q(finished_at__gte=last_3_hours))
                     )

        if since:
            since = utils.timestamp_to_datetime(since)
            pipelines = pipelines.filter(created_at__gte=since)

        paginator = pagination.DatawarehousePagination()
        paginated_pipelines = paginator.paginate_queryset(pipelines, request)
        serialized_pipelines = serializers.PipelineSimpleSerializer(paginated_pipelines, many=True).data

        context = {
            'pipelines': serialized_pipelines,
        }

        return paginator.get_paginated_response(context)


class TestSigle(APIView):
    """Endpoint for handling single test."""

    queryset = models.Test.objects.none()

    def get(self, request, test_id):
        """Return a single test."""
        try:
            test = models.Test.objects.get(id=test_id)
        except models.Test.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serialized_test = serializers.TestSerializer(test).data

        return Response(serialized_test)


class TestList(APIView):
    """Endpoint for handling many tests."""

    queryset = models.Test.objects.none()

    def get(self, request):
        """Return a list of all the tests."""
        tests = models.Test.objects.all()

        paginator = pagination.DatawarehousePagination()
        paginated_tests = paginator.paginate_queryset(tests, request)

        serialized_tests = serializers.TestSerializer(paginated_tests, many=True).data

        context = {
            'tests': serialized_tests,
        }

        return paginator.get_paginated_response(context)


class TestIssueRecord(APIView):
    """Issue records and tests."""

    queryset = models.IssueRecord.objects.none()

    def get(self, request, test_id):
        """Get list of issue records for a given test."""
        since = request.GET.get('since')

        try:
            test = models.Test.objects.get(id=test_id)
        except models.Test.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        issue_records = models.IssueRecord.objects.filter(testruns__test=test)

        if since:
            since = utils.timestamp_to_datetime(since)
            issue_records = issue_records.filter(pipeline__created_at__gte=since)

        paginator = pagination.DatawarehousePagination()
        paginated_issue_records = paginator.paginate_queryset(issue_records, request)
        serialized_issue_records = serializers.IssueRecordSerializer(paginated_issue_records, many=True).data

        context = {
            'issues': serialized_issue_records,
        }

        return paginator.get_paginated_response(context)


class TestStats(APIView):
    """Endpoint for handling test stats."""

    queryset = models.Test.objects.none()

    def get(self, request, test_id):
        """Return a test's stats."""
        since = request.GET.get('since')

        try:
            test = models.Test.objects.get(id=test_id)
        except models.Test.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        test_runs = test.testrun_set.all()
        if since:
            test_runs = test_runs.filter(pipeline__created_at__gte=since)

        issues = {}
        for issue_kind in models.IssueKind.objects.all():
            issue_records = models.IssueRecord.objects.filter(issue__kind=issue_kind, testruns__test=test)
            if since:
                issue_records = issue_records.filter(pipeline__created_at__gte=since)

            try:
                rate = issue_records.count() / test_runs.count()
            except ZeroDivisionError:
                rate = 0

            issues[issue_kind.tag] = {
                'reports': issue_records.count(),
                'rate': rate,
            }

        test_runs_failed = test_runs.filter(passed=False)

        context = {
            'test': serializers.TestSerializer(test).data,
            'total_runs': test_runs.count(),
            'total_failures': test_runs_failed.count(),
            'issues': issues,
        }
        return Response(context)


class PipelineAction(APIView):
    """PipelineAction class."""

    queryset = models.Action.objects.none()

    def post(self, request, pipeline_id):
        """Create an action."""
        action_name = request.data.get('name')
        action_name_regex = r"^([a-zA-Z_]+)+$"

        if not re.match(action_name_regex, action_name):
            return Response(f"Action does not match {action_name_regex}", status=status.HTTP_400_BAD_REQUEST)

        action_kind, _ = models.ActionKind.objects.get_or_create(name=action_name)
        pipeline = models.Pipeline.objects.get(pipeline_id=pipeline_id)

        action = models.Action.objects.create(
            kind=action_kind,
            pipeline=pipeline,
        )

        signals.action_created.send(sender=self.__class__, action=action)

        serialized_action = serializers.ActionSerializer(action).data

        return Response(serialized_action, status=status.HTTP_201_CREATED)

    def get(self, request, pipeline_id):
        """Get Actions from Pipeline."""
        pipeline = models.Pipeline.objects.get(pipeline_id=pipeline_id)
        actions = pipeline.actions.all()

        paginator = pagination.DatawarehousePagination()
        paginated_actions = paginator.paginate_queryset(actions, request)

        serialized_actions = serializers.ActionSerializer(paginated_actions, many=True).data

        context = {
            'actions': serialized_actions
        }

        return paginator.get_paginated_response(context)


class IssueRegex(APIView):
    """IssueRegex class."""

    queryset = models.IssueRegex.objects.none()

    def get(self, request):
        """Get IssueRegexes."""

        issue_regexes = models.IssueRegex.objects.all()

        paginator = pagination.DatawarehousePagination()
        paginated_issue_regexes = paginator.paginate_queryset(issue_regexes, request)

        serialized_issue_regexes = serializers.IssueRegexSerializer(paginated_issue_regexes, many=True).data

        context = {
            'issue_regexes': serialized_issue_regexes
        }

        return paginator.get_paginated_response(context)


class PipelineJobs(APIView):
    """PipelineJobs class."""

    queryset = models.Pipeline.objects.none()

    stages = {
        'lint': {'model': models.LintRun, 'serializer': serializers.LintRunSerializer},
        'merge': {'model': models.MergeRun, 'serializer': serializers.MergeRunSerializer},
        'build': {'model': models.BuildRun, 'serializer': serializers.BuildRunSerializer},
        'test': {'model': models.TestRun, 'serializer': serializers.TestRunSerializer},
    }

    def get(self, request, pipeline_id, stage_name, success=None):
        """Get jobs from selected stage."""

        stage = self.stages.get(stage_name)
        if not stage:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        try:
            pipeline = models.Pipeline.objects.get(pipeline_id=pipeline_id)
        except models.Pipeline.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        jobs = stage['model'].objects.filter(pipeline=pipeline)

        if success is not None:
            if stage_name == 'test':
                jobs = (
                    jobs.filter(passed=success, untrusted=False)
                    .select_related('test', 'beakertestrun', 'kernel_arch', 'pipeline__project')
                    .prefetch_related('test__maintainers', 'logs')
                )
            else:
                jobs = jobs.filter(success=success)

        paginator = pagination.DatawarehousePagination()
        paginated_jobs = paginator.paginate_queryset(jobs, request)
        serialized_jobs = stage['serializer'](paginated_jobs, many=True).data

        context = {
            'jobs': serialized_jobs
        }

        return paginator.get_paginated_response(context)


class PipelineFailedJobs(PipelineJobs):
    """Pipeline Failed Jobs."""

    def get(self, request, pipeline_id, stage_name):
        return super(PipelineFailedJobs, self).get(request, pipeline_id, stage_name, success=False)


class PipelinePatches(APIView):
    """PipelinePatches class."""

    queryset = models.Patch.objects.none()

    def get(self, request, pipeline_id):
        """Get patches from pipeline."""

        try:
            pipeline = models.Pipeline.objects.get(pipeline_id=pipeline_id)
        except models.Pipeline.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        patches = pipeline.patches.all()

        paginator = pagination.DatawarehousePagination()
        paginated_patches = paginator.paginate_queryset(patches, request)
        serialized_patches = serializers.PatchSerializer(paginated_patches, many=True).data

        context = {
            'patches': serialized_patches
        }

        return paginator.get_paginated_response(context)
