"""Views."""
from django.http import HttpResponseBadRequest

from rest_framework.views import APIView

from datawarehouse.models import Pipeline, BuildRun, BeakerTestRun
from datawarehouse import pagination, utils
from . import serializers


TREES_TO_PUSH = [
    'arm',
    'mainline.kernel.org',
    'net-next',
    'rdma',
    'rt-devel',
    'scsi',
    'upstream-stable',
]


class KCIDBView(APIView):
    """Endpoint for kcidb data handling."""

    queryset = Pipeline.objects.none()

    config = {
        'revisions': {
            'model': Pipeline,
            'serializer': serializers.RevisionSerializer,
            'filter': {'gittree__name__in': TREES_TO_PUSH},
            'date_filter': 'started_at__gte'
            },
        'builds': {
            'model': BuildRun,
            'serializer': serializers.BuildSerializer,
            'filter': {'pipeline__gittree__name__in': TREES_TO_PUSH},
            'date_filter': 'pipeline__started_at__gte'
            },
        'tests': {
            'model': BeakerTestRun,
            'serializer': serializers.TestSerializer,
            'filter': {
                'pipeline__gittree__name__in': TREES_TO_PUSH,
                'invalid_result': False,
                },
            'exclude': {
                'result__name': 'SKIP',
                },
            'date_filter': 'pipeline__started_at__gte',
            },
    }

    def get(self, request, kind):
        """Get a list of items of the type {kind}."""
        if kind not in self.config.keys():
            return HttpResponseBadRequest(f"Not a correct kind. Must be in {list(self.config.keys())}.")

        config = self.config[kind]
        last_retrieved_id = request.GET.get('last_retrieved_id')
        min_pipeline_started_at = request.GET.get('min_pipeline_started_at')

        if not last_retrieved_id:
            return HttpResponseBadRequest("Missing 'last_retrieved_id' parameter.")

        paginator = pagination.DatawarehousePagination()

        items = config['model'].objects.filter(id__gt=last_retrieved_id, **config['filter']).order_by('id')
        if config.get('exclude'):
            items = items.exclude(**config['exclude'])
        if min_pipeline_started_at:
            try:
                min_pipeline_started_at = utils.timestamp_to_datetime(min_pipeline_started_at)
            except ValueError as exception:
                return HttpResponseBadRequest(exception)
            items = items.filter(**{config['date_filter']: min_pipeline_started_at})

        paginated_items = paginator.paginate_queryset(items, request)
        serialized_items = config['serializer'](paginated_items, many=True).data

        # Remove these keys when it's value is None.
        keys_to_remove = ['message_id', 'log_url', 'duration', 'config_url',
                          'compiler', 'path']
        for item in serialized_items:
            for key in keys_to_remove:
                if not item.get(key):
                    item.pop(key, None)

        try:
            last_item_id = paginated_items[-1].id
        except IndexError:
            last_item_id = int(last_retrieved_id)

        context = {
            'data': {
                'version': {'major': 1, 'minor': 1},
                kind: serialized_items,
            },
            'last_retrieved_id': last_item_id
        }

        return paginator.get_paginated_response(context)
