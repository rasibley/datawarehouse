 function filterDropdown(originalFilter, selectElementId, elementsList) {
     const filter = originalFilter.toLowerCase();
     const dropdown = document.getElementById(selectElementId);

     // Remove all options first
     while (dropdown.firstChild)
         dropdown.removeChild(dropdown.firstChild);

     // Add the ones that contain the filter as a substring
     for (let element of elementsList) {
         if (
             element.tag.toLowerCase().indexOf(filter) !== -1 ||
                 element.description.toLowerCase().indexOf(filter) !== -1
         ) {
             const option = document.createElement('option');
             option.value = element.id;
             option.innerText = element.description;
             dropdown.appendChild(option);
         }
     }
 }
