"""Styles file."""
# pylint: disable=too-few-public-methods

from dataclasses import dataclass


@dataclass
class Style:
    """Encapsulates GUI styles related to task results."""

    icon: str
    icon_big: str
    cell: str
    row: str
    color: str

    def __init__(self, icon: str, color: str = '', one_cell: bool = False, colored_row: bool = False) -> None:
        """Construct a new Style class."""
        text_color = f'text-{color}' if color else ''
        table_color = f'table-{color}' if color else ''
        cell_size = 4 if one_cell else 1
        self.icon = f'<i class="{icon} {text_color}"></i>'
        self.icon_big = f'<i class="{icon} {text_color} fa-2x"></i>'
        self.cell = f'col-{cell_size} {table_color}'
        self.row = table_color if colored_row else ''
        self.color = color


_STYLE_RUNNING = Style('fas fa-hourglass-half', one_cell=True)
_STYLE_SKIP = Style('far fa-window-minimize')
_STYLE_GOOD = Style('fas fa-check', 'success')
_STYLE_WARN = Style('fas fa-exclamation', 'warning', colored_row=True)
_STYLE_BAD = Style('fas fa-poop', 'danger', colored_row=True)


def get_style(level: str, untrusted: bool = False) -> Style:
    """Return style values for template rendering."""
    if level == 'Running':
        return _STYLE_RUNNING
    if level == 'SKIP':
        return _STYLE_SKIP
    if level == 'PASS':
        return _STYLE_GOOD
    if untrusted:
        return _STYLE_WARN
    return _STYLE_BAD
