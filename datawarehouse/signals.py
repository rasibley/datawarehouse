"""Signals."""
from django import dispatch
from django.utils import timezone

from datawarehouse import utils, models


# pylint: disable=invalid-name
pipeline_processing_done = dispatch.Signal(providing_args=["pipeline"])
action_created = dispatch.Signal(providing_args=["action"])


@dispatch.receiver(pipeline_processing_done)
def send_pipeline_message(**kwargs):
    """Send pipeline message."""
    pipeline = kwargs['pipeline']

    message = {
        'timestamp': timezone.now().isoformat(),
        'status': 'finished' if pipeline.finished_at else 'running',
        'pipeline_id': pipeline.pipeline_id,
    }
    utils.send_pipeline_notification(message)


@dispatch.receiver(action_created)
def send_ready_to_report_message(**kwargs):
    """Send message to inform that a pipeline is ready to be reported."""
    action = kwargs['action']
    pipeline = action.pipeline
    triaged_kind, _ = models.ActionKind.objects.get_or_create(name='triaged')

    # Condition: The pipeline was just triaged by the first time.
    if action.kind == triaged_kind and pipeline.actions.filter(kind=triaged_kind).count() == 1:
        message = {
            'timestamp': timezone.now().isoformat(),
            'status': 'ready_to_report',
            'pipeline_id': pipeline.pipeline_id,
        }
        utils.send_pipeline_notification(message)
