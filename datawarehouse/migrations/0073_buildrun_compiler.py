# Generated by Django 2.2.10 on 2020-02-19 10:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0072_fix_test_names'),
    ]

    operations = [
        migrations.CreateModel(
            name='Compiler',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=120)),
            ],
        ),
        migrations.AddField(
            model_name='buildrun',
            name='compiler',
            field=models.ForeignKey(null=True, on_delete=models.deletion.CASCADE, to='datawarehouse.Compiler'),
        ),
    ]
