# Generated by Django 3.0.4 on 2020-03-31 17:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0083_create_messagepending'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActionKind',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Action',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('kind', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datawarehouse.ActionKind')),
                ('pipeline', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='actions', to='datawarehouse.Pipeline')),
            ],
        ),
    ]
