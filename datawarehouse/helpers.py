"""Helpers."""
from datawarehouse import models


def create_issue_record(issue_id, pipeline_id, testrun_ids=(), bot_generated=False):
    """
    Create a IssueRecord.

    issue_id:     id of the Issue.
    pipeline_id:    pipeline_id of the Pipeline.
    testrun_ids:    list of id of the affected testruns (optional).
    """
    issue = models.Issue.objects.get(id=issue_id)
    pipe = models.Pipeline.objects.get(pipeline_id=pipeline_id)
    testruns = pipe.test_jobs.filter(id__in=testrun_ids)

    issue_record = models.IssueRecord.objects.create(
        issue=issue,
        pipeline=pipe,
        bot_generated=bot_generated,
    )

    issue_record.testruns.add(*testruns)
    issue_record.save()

    return issue_record


def get_failed_pipelines(group):
    """Get a list of failed pipelines."""
    failures = (models.Pipeline.objects
                .add_stats()
                .prefetch_related('lint_jobs',
                                  'merge_jobs',
                                  'build_jobs',
                                  'test_jobs',
                                  ))

    if group == 'all':
        failures = failures.exclude(stats_lint_fail_count=0,
                                    stats_merge_fail_count=0,
                                    stats_build_fail_count=0,
                                    stats_test_fail_count=0,
                                    stats_test_error_count=0)
    elif group == 'test':
        failures = failures.exclude(stats_test_fail_count=0,
                                    stats_test_error_count=0)
    else:
        failures = failures.exclude(**{f'stats_{group}_fail_count': 0})

    return failures
