"""RabbitMQ messaging implementation."""
import pika

from datawarehouse import models


class MessageQueue:
    """Messages Queue."""

    def __init__(self, host, port):
        """Init."""
        self.conn_params = pika.ConnectionParameters(host=host, port=port)
        self.connection = None
        self.channel = None

    def _connect(self):
        """Connect."""
        self.connection = pika.BlockingConnection(self.conn_params)
        self.channel = self.connection.channel()

    def _disconnect(self):
        """Disconnect."""
        self.connection.close()

    def send(self):
        """Send messages in the queue."""
        self._connect()

        for message in models.MessagePending.objects.all():
            try:
                self.channel.basic_publish(exchange=message.exchange, routing_key='', body=message.body)
            except pika.exceptions.AMQPError:
                break
            else:
                message.delete()

        self._disconnect()

    @staticmethod
    def add(body, exchange):
        """Add message to the queue."""
        models.MessagePending.objects.create(body=body, exchange=exchange)
