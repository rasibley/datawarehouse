# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods

"""Models related to messages."""

from django.db import models


class MessagePending(models.Model):
    """Model for MessagePending."""

    body = models.CharField(max_length=200)
    exchange = models.CharField(max_length=50)

    def __str__(self):
        """Return __str__ formatted."""
        return self.body
