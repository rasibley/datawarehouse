# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.
"""Issue models file."""

from django.db import models


class IssueKind(models.Model):
    """Model for IssueKind."""

    description = models.CharField(max_length=200)
    tag = models.CharField(max_length=20)
    color = models.CharField(max_length=7, default='#dc3545')

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.description}'


class Issue(models.Model):
    """Model for Issue."""

    kind = models.ForeignKey(IssueKind, on_delete=models.CASCADE, null=True)
    description = models.CharField(max_length=200)
    ticket_url = models.URLField(unique=True)
    resolved = models.BooleanField(default=False)
    generic = models.BooleanField(default=False)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.description}'

    @property
    def pipelines(self):
        """Return all pipelines linked through records."""
        return (r.pipeline for r in self.issue_records.all())


class IssueRecord(models.Model):
    """Model for IssueRecord."""

    issue = models.ForeignKey(Issue, on_delete=models.CASCADE, related_name='issue_records')
    pipeline = models.ForeignKey('Pipeline',
                                 related_name='issue_records',
                                 on_delete=models.CASCADE)
    testruns = models.ManyToManyField('TestRun', related_name='issue_records')
    bot_generated = models.BooleanField(default=False)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.pipeline.pipeline_id} - {self.issue.description}'

    class Meta:
        """Metadata."""

        ordering = ('-id',)


class IssueRegex(models.Model):
    """Model for IssueRegex."""

    issue = models.ForeignKey(Issue, on_delete=models.CASCADE, related_name='issue_regexes')
    text_match = models.CharField(max_length=200)
    file_name_match = models.CharField(max_length=200, null=True, default=None)
    test_name_match = models.CharField(max_length=200, null=True, default=None)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.test_name_match} - {self.file_name_match} - {self.text_match}'
