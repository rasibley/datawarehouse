# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Patches file."""
import logging

from cki_lib import patchwork
from django.conf import settings

from datawarehouse import models, utils


logger = logging.getLogger(__name__)

PATCHWORK_API = patchwork.Patchwork(f'{settings.PATCHWORK_URL}/api')


def store_patchwork_series(series):
    """
    Create PatchworkSubmitter, PatchworkProject, PatchworkSeries, PatchworkPatches from patch series.

    series:      json, series from patchwork api.
    """
    submitter, created = models.PatchworkSubmitter.objects.update_or_create(
        submitter_id=series.get('submitter').get('id'),
        defaults={
            'name': series.get('submitter').get('name') or '',
            'email': series.get('submitter').get('email'),
        }
    )
    logger.info('Storing patch submitter %s (new: %s)', submitter, created)

    patchwork_project, created = models.PatchworkProject.objects.update_or_create(
        project_id=series.get('project').get('id'),
        defaults={
            'name': series.get('project').get('link_name'),
        }
    )
    logger.info('Storing patchwork project %s (new: %s)', patchwork_project, created)

    # If the series.version > 1 and patches are missing,
    # we skip this series. Mark it as skipped.
    skipped = int(series.get('version')) > 1 and not series.get('received_all')
    patchwork_series, created = models.PatchworkSeries.objects.update_or_create(
        series_id=series.get('id'),
        defaults={
            'url': series.get('url'),
            'web_url': series.get('web_url'),
            'name': series.get('name') or 'no name',
            'submitter': submitter,
            'project': patchwork_project,
            'submitted_date': utils.timestamp_to_datetime(series.get('date')),
            'skipped': skipped,
        }
    )
    logger.info('Storing patchwork series %s (new: %s)', patchwork_series, created)

    for patch in series.get('patches'):
        patchwork_patch, created = models.PatchworkPatch.objects.update_or_create(
            patch_id=patch.get('id'),
            defaults={
                'url': patch.get('url'),
                'subject': patch.get('name'),
                'web_url': patch.get('web_url'),
                'msgid': patch.get('msgid'),
                'mbox': patch.get('mbox'),
            }
        )
        patchwork_patch.series.add(patchwork_series)
        patchwork_patch.save()
        logger.info('Storing patchwork patch %s (new: %s)', patchwork_patch, created)


def store_patchwork_series_patches(patch_urls):
    """
    Create and store all the patches of a patch series.

    The patch series is determined by looking at the first patch.
    Returns only the PatchWorkPatches for the provided patch_urls.

    returns [PatchworkPatch, ] for the patch_urls
    """
    patch = PATCHWORK_API.get_patch_from_url(patch_urls[0])
    series = PATCHWORK_API.get_series(patch.get('series')[0].get('id'))
    store_patchwork_series(series)

    return list(models.PatchworkPatch.objects.filter(mbox__in=patch_urls))


def sync_patchwork_series(since_days=1):
    """
    Download new series from patchwork.

    Queries patchwork for series and stores all the patches from
    every serie retrieved.
    """
    for project in models.PatchworkProject.objects.all():
        for series in PATCHWORK_API.get_series_since(project.name, since_days=since_days):
            store_patchwork_series(series)
