# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""RC file."""
import anymarkup

from django.conf import settings
from cki_lib import gitlab


class RC(dict):
    """RC class."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(RC, self).__init__(*args, **kwargs)
        self.__dict__ = self


def filter_jobs(jobs):
    """
    Filter jobs to remove retried jobs.

    Only keep the job with the higher id for each job name.
    """
    jobs_filtered = {}
    for job in jobs:
        if jobs_filtered.get(job.name):
            if jobs_filtered.get(job.name).id > job.id:
                continue
        jobs_filtered[job.name] = job

    return jobs_filtered.values()


def order_jobs(jobs):
    """
    Order jobs according to stages order.

    We need to order the jobs. Otherwise, retried jobs will be submitted at last
    because it's job id is higher.

    An example is a failed build which comes after the publish, and publish expects
    to have a corresponding build already saved.
    We have to fix this on the submit_pipeline function and then remove this :)
    """
    job_order = ['lint', 'merge', 'createrepo', 'build', 'publish', 'test']
    jobs_ordered = [job for stage in job_order for job in jobs if job.stage == stage]
    return jobs_ordered


def get_rcs(gitlab_project_id, pipeline_id):
    """Get rc files from gitlab pipeline."""
    gitlab_helper = gitlab.GitlabHelper(settings.GITLAB_URL, settings.GITLAB_TOKEN)
    gitlab_helper.set_project(gitlab_project_id)

    pipeline = gitlab_helper.project.pipelines.get(pipeline_id)
    jobs = pipeline.jobs.list(as_list=False)

    jobs = filter_jobs(jobs)
    jobs = order_jobs(jobs)

    for job in jobs:
        if job.attributes.get('artifacts'):
            rc_file = gitlab_helper.get_artifact(job.attributes['id'], 'rc', raw=True)
            if rc_file:
                rc_data = anymarkup.parse(rc_file, 'ini')
                yield job, RC(rc_data)
